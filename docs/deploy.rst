Deployment
==========

There are several ways to start WatchGhost. You can use a systemd service 
or a uWSGI file for example. And serve it through a web server like Nginx.

uWSGI
-----

A uWSGI file looks like that:

.. code-block:: ini

  [uwsgi]
  attach-daemon = /path/to/your/watchghost --config=/path/to/your/config

Systemd service
---------------

We propose a ready to use Systemd setting.

Place the following content in ``watchghost.service`` in the Systemd unit search path (usually ``/usr/lib/systemd/system/watchghost.service``, but it may change given your distribution) :

.. literalinclude:: ../watchghost/deployment/watchghost.service

Place the following content in a file in a Sysusers directory (usually ``/usr/lib/sysusers.d/watchghost.conf``, but it may change given your distribution) :

.. literalinclude:: ../watchghost/deployment/sysusers.conf

Nginx
-----

And a Nginx configuration looks like something like that:

.. code-block::

  server {
      listen 80;
      listen [::]:80;
      server_name your_monitoring_url;

      location / {
          return 301 https://your_monitoring_url$request_uri;
      }
  }

  server {
      listen 443 ssl http2;
      listen [::]:443 ssl http2;
      server_name your_monitoring_url;
      access_log access_log main;
      error_log error_log info;

      ssl_certificate fullchain.pem;
      ssl_certificate_key privkey.pem;
      ssl_protocols TLSv1.2 TLSv1.1 TLSv1;

      location ^~ /static/ {
        root /path/to/your/watchghost/installation/;
      }

      location /websocket {
        proxy_set_header Host $host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_http_version 1.1;
        proxy_pass http://127.0.0.1:8888;
      }

      location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8888;
      }
  }
