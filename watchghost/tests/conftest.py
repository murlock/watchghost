from uuid import uuid4

import pytest
import toml
from aiohttp import web
from watchghost import config
from watchghost import web as wg_web


@pytest.fixture
def create_app(loop):
    app = web.Application()
    config.read('/tmp/pytest/{}'.format(uuid4()))
    app.add_routes(wg_web.routes)
    return app


@pytest.fixture
def default_groups():
    return {'all': ['localhost']}


@pytest.fixture
def default_loggers():
    return {'console': {'type': 'Console'}}


@pytest.fixture
def default_servers():
    return {'localhost': {
        'name': 'localhost', 'ipv4': '127.0.0.1', 'ipv6': '::1'
    }}


@pytest.fixture
def default_watchers():
    return {'local': {
        'description': 'http://localhost:8888/',
        'service': 'network.HTTP',
        'server': 'localhost',
        'url': 'http://localhost:8888/'
    }}


@pytest.fixture
def config_factory(
        tmpdir,
        default_groups, default_loggers, default_servers, default_watchers
):
    def create_config_dir(configs=None):
        config_dir = tmpdir.mkdir("config")
        if configs is None:
            configs = {}
        configs.setdefault('groups', default_groups)
        configs.setdefault('loggers', default_loggers)
        configs.setdefault('servers', default_servers)
        configs.setdefault('watchers', default_watchers)
        for config_name, config_params in configs.items():
            tmp_file = config_dir.join('{}.toml'.format(config_name))
            tmp_file.write(toml.dumps(config_params))
        return str(config_dir), configs

    return create_config_dir
