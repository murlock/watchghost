from unittest import mock

from watchghost.config import read


@mock.patch('watchghost.config.Logger.create')
@mock.patch('watchghost.config.Server')
@mock.patch('watchghost.config.Watcher')
@mock.patch('watchghost.config.Service')
@mock.patch(
    'watchghost.config.app',
    services={}, watchers={}, servers={}, loggers=[]
)
def test_read_create_logger_server_service_watcher_default(
        app_mock, service_mock, watcher_mock,
        server_mock, create_logger_mock, config_factory
):
    config_dir, configs = config_factory()
    read(config_dir)
    create_logger_mock.assert_has_calls([
        mock.call(config) for config in configs['loggers'].values()
    ])
    server_mock.assert_has_calls([
        mock.call(name, config) for name, config in configs['servers'].items()
    ])
    service_mock.assert_has_calls([
        mock.call(
            config.get('service'),
            group=config.get('group'),
            server=config.get('server')
        )
        for name, config in configs['watchers'].items()
    ])
    watcher_mock.assert_has_calls([
        mock.call(
            name,
            app_mock.servers['localhost'],
            app_mock.services['network.HTTP'],
            config,
            app_mock.loggers
        )
        for name, config in configs['watchers'].items()
    ])


@mock.patch('watchghost.config.Logger.create')
@mock.patch('watchghost.config.Server')
@mock.patch('watchghost.config.Watcher')
@mock.patch('watchghost.config.Service')
@mock.patch(
    'watchghost.config.app',
    services={}, watchers={}, servers={}, loggers=[]
)
def test_read_uses_name_if_no_description_provided_for_watcher(
        app_mock, service_mock, watcher_mock,
        server_mock, create_logger_mock, config_factory
):
    config_dir, configs = config_factory({'watchers': {
        'local': {
            'service': 'network.HTTP',
            'server': 'localhost',
            'url': 'http://localhost:8888/'
        }
    }})
    read(config_dir)
    watcher_mock.assert_has_calls([
        mock.call(
            name,
            app_mock.servers['localhost'],
            app_mock.services['network.HTTP'],
            {**config, 'description': 'local'},
            app_mock.loggers
        )
        for name, config in configs['watchers'].items()
    ])
