async def test_api_get_servers(aiohttp_client, create_app):
    client = await aiohttp_client(create_app)
    resp = await client.get('/api/servers/')
    assert resp.status == 200
    data = await resp.json()
    assert len(data['objects']) == 1
    assert 'uuid' not in data['objects'][0]
    assert data['objects'][0]['id'] == 'localhost'
    assert data['objects'][0]['config']['name'] == 'localhost'
    assert data['objects'][0]['config']['ipv4'] == '127.0.0.1'
    assert data['objects'][0]['config']['ipv6'] == '::1'


async def test_api_get_groups(aiohttp_client, create_app):
    client = await aiohttp_client(create_app)
    resp = await client.get('/api/groups/')
    assert resp.status == 200
    data = await resp.json()
    assert len(data['objects']) == 1
    assert 'members' in data['objects'][0]
    assert data['objects'][0]['name'] == 'all'


async def test_api_get_loggers(aiohttp_client, create_app):
    client = await aiohttp_client(create_app)
    resp = await client.get('/api/loggers/')
    assert resp.status == 200
    data = await resp.json()
    assert len(data['objects']) == 1
    assert data['objects'][0]['type'] == \
        '<class \'watchghost.loggers.Console\'>'


async def test_api_get_watchers(aiohttp_client, create_app):
    client = await aiohttp_client(create_app)
    resp = await client.get('/api/watchers/')
    assert resp.status == 200
    data = await resp.json()

    assert len(data['objects']) == 16

    assert 'uuid' not in data['objects'][0]
    assert data['objects'][0]['id'] == 'localhost_ping4'

    assert 'uuid' not in data['objects'][0]['server']
    assert data['objects'][0]['server']['id'] == 'localhost'
    assert data['objects'][0]['server']['config']['name'] == 'localhost'
    assert data['objects'][0]['server']['config']['ipv4'] == '127.0.0.1'
    assert data['objects'][0]['server']['config']['ipv6'] == '::1'

    assert 'uuid' not in data['objects'][0]['service']
    assert data['objects'][0]['service']['name'] == 'network.Ping'
    assert 'config' in data['objects'][0]['service']
    assert data['objects'][0]['service']['description'] is None
    assert data['objects'][0]['service']['group'] == 'all'
    assert data['objects'][0]['service']['server'] is None

    assert 'status' in data['objects'][0]
    assert 'last_result' in data['objects'][0]
    assert data['objects'][0]['description'] == 'Ping IPv4'
    assert 'next_check_hour' in data['objects'][0]
    assert data['objects'][0]['tags'] == {}


async def test_api_get_services(aiohttp_client, create_app):
    client = await aiohttp_client(create_app)
    resp = await client.get('/api/services/')
    assert resp.status == 200
    data = await resp.json()
    assert len(data['objects']) == 3

    assert 'uuid' not in data['objects'][0]
    assert data['objects'][0]['name'] == 'network.Ping'
    assert data['objects'][0]['config'] == {
        'description': None,
        'repeat': 60,
        'before': '23:59:59',
        'after': '00:00:00',
        'status': {'info': [{'ok': True}], 'critical': [{}]},
        'retry': 2,
        'retry_interval': 15,
        'timeout': 3,
        'ip_version': 4,
        'ping_command': 'ping',
        'ping6_command': 'ping -6'
    }
    assert data['objects'][0]['description'] is None
    assert data['objects'][0]['group'] == 'all'
    assert data['objects'][0]['server'] is None
