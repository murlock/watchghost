==========
WatchGhost
==========

.. image:: https://readthedocs.org/projects/watchghost/badge/?version=latest&style=plastic

Your invisible but loud monitoring pet

Quickstart
==========

To install Watchghost, you need to specify the extras required for watchers and loggers you want to use.

.. code-block:: shell

  # example: to install required dependencies
  # for SSH watcher and InfluxDB logger
  pip install watchghost[influx,ssh]
  watchghost
  $NAVIGATOR http://localhost:8888
  $EDITOR ~/.config/watchghost/*

Code on watchghost
==================

.. code-block:: shell

  git clone https://gitlab.com/localg-host/watchghost.git
  cd watchghost
  pip install -e .[all]
  python -m watchghost
